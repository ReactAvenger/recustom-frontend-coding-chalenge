import React from "react";
import Toast, { ToastType } from "./Toast";

export default {
  title: "Components/Notification",
  component: Notification,
  parameters: {
    layout: 'centered',
  },
};

const Template = (args) => <Toast {...args} />;

export const SuccessNoAction = Template.bind({});
SuccessNoAction.args = {
  type: ToastType.SUCCESS,
  message: "The action that you have done was a success! Well done",
};

export const SuccessWithActionLong = Template.bind({});
SuccessWithActionLong.args = {
  type: ToastType.SUCCESS,
  message: "Success",
  description:
    "Well done, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.",
  action: true,
};

export const WarningNoAction = Template.bind({});
WarningNoAction.args = {
  type: ToastType.WARNING,
  message:
    "The file <strong>flowbite-figma-pro</strong> was permanently deleted.",
};

export const WarningWithActionLong = Template.bind({});
WarningWithActionLong.args = {
  type: ToastType.WARNING,
  message: "Attention",
  description:
    "Oh snap, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.",
  action: true,
};

export const AvatarWithNameAndMessage = Template.bind({});
AvatarWithNameAndMessage.args = {
  type: ToastType.AVATAR,
  message: "Bonnie Green",
  description: "Hi Neil, thanks for sharing your thoughts regarding Flowbite.",
  action: true,
};

export const SuccessNoActionMobile = Template.bind({});
SuccessNoActionMobile.args = {
  type: ToastType.SUCCESS,
  message: "The action that you have done was a success! Well done",
  mobile: true,
};

export const SuccessWithActionLongMobile = Template.bind({});
SuccessWithActionLongMobile.args = {
  type: ToastType.SUCCESS,
  message: "Success",
  description:
    "Well done, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.",
  action: true,
  mobile: true,
};

export const WarningNoActionMobile = Template.bind({});
WarningNoActionMobile.args = {
  type: ToastType.WARNING,
  message:
    "The file <strong>flowbite-figma-pro</strong> was permanently deleted.",
  mobile: true,
};

export const WarningWithActionLongMobile = Template.bind({});
WarningWithActionLongMobile.args = {
  type: ToastType.WARNING,
  message: "Attention",
  description:
    "Oh snap, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.",
  action: true,
  mobile: true,
};

export const AvatarWithNameAndMessageMobile = Template.bind({});
AvatarWithNameAndMessageMobile.args = {
  type: ToastType.AVATAR,
  message: "Bonnie Green",
  description: "Hi Neil, thanks for sharing your thoughts regarding Flowbite.",
  action: true,
  mobile: true,
};
